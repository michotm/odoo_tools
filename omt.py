#!/usr/bin/env python3
import fire
from gevent import joinall
import logging
import os
from pssh.clients.native import SSHClient

from pssh.exceptions import Timeout
import subprocess
import time
import yaml

class Omt(object):
    cfg_options = False
    cfg_server_prod = False

    def __init__(self, config):
        self.cfg_options = config['options']
        self.cfg_servers = config['servers']

    def _kill_db_sessions(self, db=False):
        # kill opened sessions
        print("_kill_db_sessions {db}".format(db=db))
        server2manage = self.get_server_config('local')
        if not db:
            db = server2manage['db']
        cmd = ['psql', '-U', 'odoo',
               '-d', 'template1', '-c',
               "SELECT pg_terminate_backend(pg_stat_activity.pid) \
                FROM pg_stat_activity \
                WHERE pg_stat_activity.datname = '{db}' \
                AND pid <> pg_backend_pid();".format(db=db)]

        subprocess.run(cmd)

    def reset_password(self, db, db_user='odoo', user2reset='admin', new_passwd=None):
        """
        reset password of 1 or more users in sql mode
        :param db_user: user allowed to write on res_users table
        :param user2reset: login of the user to reset
        :param new_passwd: crypted password
        :return:
        """
        if new_passwd is None:
            new_passwd = '$pbkdf2-sha512$25000$CIHQ.p.zNubcu7f2vleKEQ$hT8aI1hFDPtA6CU33' \
                         'ZLUy2WON1n2zlInEeh9/9wJfFD9nT5Bb6k5oUn9ku/Z1AfFh4sgZCcdrdmyAW.n614lQg'
        cmd = ['psql', '-U', db_user,
               '-d', db, '-c',
               "update res_users \
                set password_crypt = '{new_passwd}' \
                where login='{user2reset}';".format(db_user=db_user,
                                                 new_passwd=new_passwd,
                                                 user2reset=user2reset)]
        print(cmd)

        subprocess.run(cmd)

    def clean_db_with_sql(self, db, db_user='odoo'):
        """
        clean DB in SQL to avoid some unintended action like schedul job
        :param db_user: user allowed to write on res_users table
        :param user2reset: login of the user to reset
        :param new_passwd: crypted password
        :return:
        """
        # disable the cron job
        cmd = ['psql', '-U', db_user,
               '-d', db, '-c',
               "update ir_cron set active = False;"]

        print(cmd)

        subprocess.run(cmd)


    def get_server_config(self, server):
        if server not in self.cfg_servers:
            error_msg = "Error: server {server} not configured\n" \
                        "configured server : {servers}"
            print(error_msg.format(server=server,
                                   servers=[s for s in self.cfg_servers]))
            return False
        return self.cfg_servers[server]

    # 
    # Drop DB but only on local server
    # db : name of the DB to drop !!!! can nerver contain PROD
    def drop_db(self, db='dev'):
        print("Drop DB {db}".format(db=db))
        server2manage = self.get_server_config('local')
        if not db:
            db = server2manage['db']  

        # check if name contain prod
        if 'prod' in db.lower():
            print("Not allowed to Drop PROD DB : {db_name}!!!".format(db_name=db))
            return 0

        self._kill_db_sessions(db=db)
        sql_drop = ["dropdb",
                    "-U",
                    server2manage['db_user'],
                    "--if-exists",
                    db
                    ]
        print(" ".join(sql_drop))
        r = subprocess.run(sql_drop)
        return r

    # 
    # Restore DB but only on local server
    # db : name of the DB to restore !!!! can nerver contain PROD
    # by default drop DB before
    def restore_db(self, db='dev', bk_db=False, bk_fs=False, drop_if_exist=True):
        print("Restore DB {db}".format(db=db))
        server2manage = self.get_server_config('local')
        if not db:
            db = server2manage['db']  

        self.drop_db(db)

        sql_create = ["createdb",
                      "-U",
                      server2manage['db_user'],
                      "-O", server2manage['db_user'], db 
                     ]
        print(" ".join(sql_create))
        r = subprocess.run(sql_create)

        sql_restore = ['pg_restore',
                       '--no-owner',
                       '-U',
                       server2manage['db_user'],
                       "-d", db,
                       bk_db]
        print(" ".join(sql_restore))
        r = subprocess.run(sql_restore)

        new_file_store_path = os.path.join(server2manage['file_store_path'], db)
        if len(new_file_store_path) < 10:
            return False
        cmd = ['rm', '-Rf', new_file_store_path]
        print(" ".join(cmd))
        r = subprocess.run(cmd)
        cmd = ['mkdir', '-p', new_file_store_path]
        print(" ".join(cmd))
        r = subprocess.run(cmd)
        cmd = ['tar', '-zxvf', bk_fs,
               '-C', new_file_store_path]
        print(" ".join(cmd))
        r = subprocess.run(cmd)

        return r

    def backup(self, server_from=None, db=False):
        if not server_from:
            server_from = 'prod'
        """Backup the specified db."""        
        logging.debug('Starting Backup server')             
        
        bk_time = time.strftime("%Y%m%d-%H%M%S")

        server2backup = self.get_server_config(server_from)
        if not db:
            db = server2backup['db']  
        client = SSHClient(server2backup['host'], 
                           user=server2backup['user'], 
                           password=server2backup['passwd'])
        print("SSH con with:{}".format((server2backup['host'],
                                        server2backup['user'],
                                        server2backup['passwd'])))
        print(client)

        cmd = ['pg_dump', 
               '--no-owner',
               '--format=c',
               '-U {user}'.format(user=server2backup['db_user'])
               ]
        # add host if not localhost
        if server2backup['db_host'] != 'localhost':
            cmd.append('--host={host}'.format(host=server2backup['db_host']))
        
        cmd.append(db)
        bk_db = 'dump_{server}_{dt}.dump'.format(server=server_from,
                                                 dt=bk_time)
        bk_db_full_path = os.path.join(self.cfg_options['tmp_path'], bk_db)
        cmd.insert(-1, '--file={}'.format(bk_db_full_path))
        #r = os.system(" ".join(cmd))
        print(" ".join(cmd))
        channel, host, stdout, stderr, stdin = client.run_command(" ".join(cmd))
        client.wait_finished(channel)

        #tar filestore
        bk_fs = 'file_store_{server}_{dt}.tar.gz'.format(server=server_from,
                                                         dt=bk_time)
        bk_fs_full_path = os.path.join(self.cfg_options['tmp_path'], bk_fs)
        fs2bk = db
        fs2bk_full_path = os.path.join(server2backup['file_store_path'], fs2bk)                                                         
        cmd = ['tar', 
               '-C', fs2bk_full_path,
               '-zcf',
               bk_fs_full_path,
               './'
               ]
        print(" ".join(cmd))
        channel, host, stdout, stderr, stdin = client.run_command(" ".join(cmd))
        client.wait_finished(channel)  
        # Copy from backuped Server 
        print("copy_remote_file {remote} {dest}".format(remote=bk_db_full_path, 
                                                        dest=bk_db_full_path))
        
        greenlets = client.copy_remote_file(bk_db_full_path, bk_db_full_path)
        print("join all")
        joinall(greenlets, raise_error=True)
        print("copy_remote_file {remote} {dest}".format(remote=bk_fs_full_path, 
                                                        dest=bk_fs_full_path))
        greenlets = client.copy_remote_file(bk_fs_full_path, bk_fs_full_path)
        print("join all")
        joinall(greenlets, raise_error=True)                                                        

        
        return (bk_db_full_path,bk_fs_full_path)

    def service_manager(self, service='odoo-server', action='status', db=None):
        print("service_manager {service} {action} {db}".format(service=service,
                                                               action=action,
                                                               db=db or ''))
        server2manage = self.get_server_config('local')
        if not db:
            db = server2manage['db'] 
        if server2manage['service'] and action != 'update':
            manager = ["sudo","service",
                        service,
                        action,                 
                    ]
            print(" ".join(manager))
            r = subprocess.run(manager) 
        else:
            if action == 'stop':
                cmd = ['pkill', '-f', 'python.*odoo-bin']
                r = subprocess.run(cmd)
            elif action == 'start':
                cmd = [server2manage['odoo-bin'], 
                       '-c', server2manage['odoo_c'],
                       '-d', db,
                      ]
                r = subprocess.Popen(cmd)
            elif action == 'update':
                cmd = [server2manage['odoo-bin'], 
                       '-c', server2manage['odoo_c'],
                       '-d', db,
                      ]
                if 'odoo_u' in server2manage and server2manage['odoo_u']:
                    cmd += ['-u', server2manage['odoo_u']]
                if 'odoo_i' in server2manage and server2manage['odoo_i']:
                    cmd += ['-i', server2manage['odoo_i']]
                cmd += ['--stop-after-init']
                print("--{}--".format(cmd))
                print(" ".join(cmd))
                r = subprocess.run(cmd)

        return True

    def clean_db(self, db=False, admin_user='admin', admin_pass='toto'):
        server2manage = self.get_server_config('local')
        if not db:
            db = server2manage['db'] 
        import odoorpc
        print('OdooRPC connect to server : {db} {host} {port} {admin_user} {admin_pass}'.format(db=db,
                                                                      host=server2manage['host'],
                                                                      port=server2manage['port'],
                                                                      admin_user=admin_user,
                                                                      admin_pass=admin_pass))
        odoo = odoorpc.ODOO(server2manage['host'], port=server2manage['port'])
        odoo.login(db, admin_user, admin_pass)

        # reset password
        print("Reset Password")
        users = odoo.env['res.users']
        user_ids = users.search([('login', '!=', admin_user),
                                 ('id', '>', 1)])
        users.browse(user_ids).write({'password': self.cfg_options['reset_passwd'],
                                      'email': self.cfg_options['reset_email']})

        print("Disable Mail Server")
        # disable mail server
        fetchmail_servers = odoo.env['fetchmail.server']
        fetchmail_server_ids = fetchmail_servers.search([])
        fetchmail_servers.browse(fetchmail_server_ids).write({'active': False})
        
        # delete outgoing mail server
        print("Disable Outgoing Mail Server")
        outgoing_servers = odoo.env['ir.mail_server']
        outgoing_server_ids = outgoing_servers.search([])
        outgoing_servers.unlink(outgoing_server_ids)

        # cron action
        print("Disable Cron")
        ir_crons = odoo.env['ir.cron']
        ir_cron_ids = ir_crons.search([('id', '>', 1)])
        if ir_cron_ids:
            ir_crons.browse(ir_cron_ids).write({'active': False})

        # report URL
        ir_configs = odoo.env['ir.config_parameter']
        ir_config_ids = ir_configs.search([('key', '=', 'report.url')])
        if ir_config_ids:
            report_url = "http://{host}:{port}".format(host=server2manage['host'],
                                                       port=server2manage['port'])
            ir_configs.browse(ir_config_ids).write({'value': report_url})
        # Clean Database UID
        ir_config_ids = ir_configs.search([('key', '=', 'database.uuid')])
        ir_configs.browse(ir_config_ids).write({'value': 'toto'})

        # reset ADMIN password
        if 'reset_admin_passwd' in self.cfg_options and self.cfg_options['reset_admin_passwd']:
            if 'reset_admin_login' in self.cfg_options and self.cfg_options['reset_admin_login']:
                login_list = self.cfg_options['reset_admin_login'].split(',')
                user_ids = users.search([('login', 'in', login_list),
                                        ])
                users.browse(user_ids).write({'password': self.cfg_options['reset_admin_passwd'],
                                            'email': self.cfg_options['reset_email']})


       
    def save_template(self, db=False):
        print("Save template {db}".format(db=db))
        server2manage = self.get_server_config('local')
        if not db:
            db = server2manage['db']
        template_db = db + "_template"
        self.drop_db(db=template_db)
        # kill session on main DB to be able to use it as template source
        self._kill_db_sessions(db=db)
        sql_create = ["createdb",
                      "-U",
                      server2manage['db_user'],
                      "-O", server2manage['db_user'], template_db,
                      '-T', db 
                     ]
        print(" ".join(sql_create))
        r = subprocess.run(sql_create)

        # copy filestore
        new_file_store_path = os.path.join(server2manage['file_store_path'], template_db)
        if len(new_file_store_path) < 10:
            return False
        cmd = ['rm', '-Rf', new_file_store_path]
        print(" ".join(cmd))
        r = subprocess.run(cmd)
        cmd = ['cp', '-R', 
                os.path.join(server2manage['file_store_path'], db),
                new_file_store_path]
        print(" ".join(cmd))
        r = subprocess.run(cmd)

    def reset_from_template(self, db=False):
        server2manage = self.get_server_config('local')
        # stop Odoo Service
        self.service_manager(action='stop', db=db)
        if not db:
            db = server2manage['db']
        template_db = db + "_template"
        self.drop_db(db=db)
        sql_create = ["createdb",
                      "-U",
                      server2manage['db_user'],
                      "-O", server2manage['db_user'], db,
                      '-T', template_db 
                     ]
        print(" ".join(sql_create))
        r = subprocess.run(sql_create)

        # copy filestore
        new_file_store_path = os.path.join(server2manage['file_store_path'], db)
        if len(new_file_store_path) < 10:
            return False
        cmd = ['rm', '-Rf', new_file_store_path]
        print(" ".join(cmd))
        r = subprocess.run(cmd)
        cmd = ['cp', '-R', 
                os.path.join(server2manage['file_store_path'], template_db),
                new_file_store_path]
        print(" ".join(cmd))
        r = subprocess.run(cmd)
        # Restart service
        self.service_manager(action='start', db=db)
        time.sleep(10)

    def duplicate_from_template(self, db=False, to_db=False):
        server2manage = self.get_server_config('local')
        # stop Odoo Service
        self.service_manager(action='stop', db=db)
        if not db:
            db = server2manage['db']
        template_db = db + "_template"
        self.drop_db(db=to_db)
        sql_create = ["createdb",
                      "-U",
                      server2manage['db_user'],
                      "-O", server2manage['db_user'], to_db,
                      '-T', template_db
                     ]
        print(" ".join(sql_create))
        r = subprocess.run(sql_create)

        # copy filestore
        new_file_store_path = os.path.join(server2manage['file_store_path'], to_db)
        if len(new_file_store_path) < 10:
            return False
        cmd = ['rm', '-Rf', new_file_store_path]
        print(" ".join(cmd))
        r = subprocess.run(cmd)
        cmd = ['cp', '-R',
                os.path.join(server2manage['file_store_path'], template_db),
                new_file_store_path]
        print(" ".join(cmd))
        r = subprocess.run(cmd)
        # Restart service
        self.service_manager(action='start', db=to_db)
        time.sleep(10)

    def copy2loc(self, server_from=None, db_from=None, db=None, admin_user=None, admin_pass=None):
        if not server_from:
            server_from = 'prod'
        server2bk = self.get_server_config(server_from)
        # stop Odoo Service
        self.service_manager(action='stop', db=db)
        # Backup DB and File-store
        bk_db,bk_fs = self.backup(server_from=server_from, db=db_from)
        # Restore DB localy and drop old db if needed
        self.restore_db(db=db, bk_db=bk_db, bk_fs=bk_fs)
        # clean db in sql before starting odoo
        self.clean_db_with_sql(db=db)
        # start Odoo Service
        self.service_manager(action='update', db=db)
        time.sleep(10)
        self.service_manager(action='start', db=db)
        time.sleep(10)
        # Clean DB - Install rebon reset password ....
        if admin_user is None:
            admin_user = server2bk['odoo_admin_user']
        self.reset_password(db=db, user2reset=admin_user)
        self.clean_db(db=db, admin_user=admin_user)
        self.save_template(db=db)
        
def main():
    with open("./config.yaml", 'r') as stream:
        try:
            config=yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    fire.Fire(Omt(config))

if __name__ == '__main__':
  main()
