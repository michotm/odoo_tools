Odoo Management Tools
===================

This project aims to deal with Odoo instance with duplicate instance (prod2dev) in mind.

- Copy a remote DB in local
- Clean DB (Reset password, disable mail server, )
    - Reset password
    - Disable mail server
    - install module like web_environment_ribbon

Quick Start
======
1. Install
   ```
   git clone git@masthercell_be/odoo/server-tools omt
   pip install -r requirements.txt
   ```
2. Config
   ```
   cp config.tmpl.yaml config.yaml
   ```
   _edit config.yaml to adapt it to your config_

3. Prod2Dev 
   ```
   #Copy Prod to local (dev or val) + clean DB + save template
   python3 omt.py prop2loc [--DB=....]
   ```
4. Restore state of last Copy 
   ```
   #Restore last template
   python3 omt.py reset_from_template [--DB=....]
   ```
5. Save template after modif 
   ```
   #Save template after modifications
   python3 omt.py save_template [--DB=....]
   ```

